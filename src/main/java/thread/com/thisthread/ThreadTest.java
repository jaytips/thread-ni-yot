package thread.com.thisthread;

public class ThreadTest extends Thread {
	
	public ThreadTest(String str) {
		super(str);
		//System.out.println(str);
	}
	
	int n  = 0,eggs;
	
	public void run() {
		
		for (int i = 0; i < 7; i++) {
			 n = i +1;
			 int itlog = 0;
			 
			System.out.println("\n--------"+"Day " + n + "--------\n");
			
			for(int j = 0;j <5;j++) {
					
					int egg = getRandomNumber(); //calling method
					
					if(egg<20) {
						eggs =( 20 - egg);
					}else if(egg ==20) {
						eggs = 0;
					}
					itlog = itlog + egg;
					System.out.println("Client: "+ j + " gets " +egg+ " eggs remaing " + eggs);
				
			}
			System.out.println("Eggs produced: " + itlog);
			
		}
		
	}
	
	public static int getRandomNumber() {// getRandomNumber of egg
		
		int min = 18,max =20;
		
		if(min >=max) {
				throw new IllegalArgumentException("max is greater than min");
		}
		
		return (int)(Math.random()*((max - min) + 1)) + min;
	}
	
//	public static int getRandomNumberEgg() {
//			
//			int min =90 ,max =120;
//			
//			if(min >=max) {
//					throw new IllegalArgumentException("max is greater than min");
//			}
//			
//			return (int)(Math.random()*((max - min) + 1)) + min;
//		}
}
